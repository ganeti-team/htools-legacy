.TH HSPACE 1 "" "htools" "Ganeti H-tools"
.SH NAME
.PP
hspace - Cluster space analyzer for Ganeti
.SH SYNOPSIS
.PP
\f[B]hspace\f[] {backend options...} [algorithm options...]
[request options...] [ -p [\f[I]fields\f[]] ] [-v... | -q]
.PP
\f[B]hspace\f[] --version
.PP
Backend options:
.PP
{ \f[B]-m\f[] \f[I]cluster\f[] | \f[B]-L[\f[] \f[I]path\f[]
\f[B]] [-X]\f[] | \f[B]-t\f[] \f[I]data-file\f[] |
\f[B]--simulate\f[] \f[I]spec\f[] }
.PP
Algorithm options:
.PP
\f[B][ --max-cpu \f[I]cpu-ratio\f[] ]\f[]
\f[B][ --min-disk \f[I]disk-ratio\f[] ]\f[]
\f[B][ -O \f[I]name...\f[] ]\f[]
.PP
Request options:
.PP
\f[B][--memory\f[] \f[I]mem\f[] \f[B]]\f[] \f[B][--disk\f[]
\f[I]disk\f[] \f[B]]\f[] \f[B][--req-nodes\f[] \f[I]req-nodes\f[]
\f[B]]\f[] \f[B][--vcpus\f[] \f[I]vcpus\f[] \f[B]]\f[]
\f[B][--tiered-alloc\f[] \f[I]spec\f[] \f[B]]\f[]
.SH DESCRIPTION
.PP
hspace computes how many additional instances can be fit on a
cluster, while maintaining N+1 status.
.PP
The program will try to place instances, all of the same size, on
the cluster, until the point where we don\[aq]t have any N+1
possible allocation. It uses the exact same allocation algorithm as
the hail iallocator plugin.
.PP
The output of the program is designed to interpreted as a shell
fragment (or parsed as a \f[I]key=value\f[] file). Options which
extend the output (e.g. -p, -v) will output the additional
information on stderr (such that the stdout is still parseable).
.PP
The following keys are available in the output of the script (all
prefixed with \f[I]HTS_\f[]):
.TP
.B SPEC_MEM, SPEC_DSK, SPEC_CPU, SPEC_RQN
These represent the specifications of the instance model used for
allocation (the memory, disk, cpu, requested nodes).
.RS
.RE
.TP
.B CLUSTER_MEM, CLUSTER_DSK, CLUSTER_CPU, CLUSTER_NODES
These represent the total memory, disk, CPU count and total nodes
in the cluster.
.RS
.RE
.TP
.B INI_SCORE, FIN_SCORE
These are the initial (current) and final cluster score (see the
hbal man page for details about the scoring algorithm).
.RS
.RE
.TP
.B INI_INST_CNT, FIN_INST_CNT
The initial and final instance count.
.RS
.RE
.TP
.B INI_MEM_FREE, FIN_MEM_FREE
The initial and final total free memory in the cluster (but this
doesn\[aq]t necessarily mean available for use).
.RS
.RE
.TP
.B INI_MEM_AVAIL, FIN_MEM_AVAIL
The initial and final total available memory for allocation in the
cluster. If allocating redundant instances, new instances could
increase the reserved memory so it doesn\[aq]t necessarily mean the
entirety of this memory can be used for new instance allocations.
.RS
.RE
.TP
.B INI_MEM_RESVD, FIN_MEM_RESVD
The initial and final reserved memory (for redundancy/N+1
purposes).
.RS
.RE
.TP
.B INI_MEM_INST, FIN_MEM_INST
The initial and final memory used for instances (actual runtime
used RAM).
.RS
.RE
.TP
.B INI_MEM_OVERHEAD, FIN_MEM_OVERHEAD
The initial and final memory overhead--memory used for the node
itself and unacounted memory (e.g. due to hypervisor overhead).
.RS
.RE
.TP
.B INI_MEM_EFF, HTS_INI_MEM_EFF
The initial and final memory efficiency, represented as instance
memory divided by total memory.
.RS
.RE
.TP
.B INI_DSK_FREE, INI_DSK_AVAIL, INI_DSK_RESVD, INI_DSK_INST, INI_DSK_EFF
Initial disk stats, similar to the memory ones.
.RS
.RE
.TP
.B FIN_DSK_FREE, FIN_DSK_AVAIL, FIN_DSK_RESVD, FIN_DSK_INST, FIN_DSK_EFF
Final disk stats, similar to the memory ones.
.RS
.RE
.TP
.B INI_CPU_INST, FIN_CPU_INST
Initial and final number of virtual CPUs used by instances.
.RS
.RE
.TP
.B INI_CPU_EFF, FIN_CPU_EFF
The initial and final CPU efficiency, represented as the count of
virtual instance CPUs divided by the total physical CPU count.
.RS
.RE
.TP
.B INI_MNODE_MEM_AVAIL, FIN_MNODE_MEM_AVAIL
The initial and final maximum per-node available memory. This is
not very useful as a metric but can give an impression of the
status of the nodes; as an example, this value restricts the
maximum instance size that can be still created on the cluster.
.RS
.RE
.TP
.B INI_MNODE_DSK_AVAIL, FIN_MNODE_DSK_AVAIL
Like the above but for disk.
.RS
.RE
.TP
.B TSPEC
If the tiered allocation mode has been enabled, this parameter
holds the pairs of specifications and counts of instances that can
be created in this mode. The value of the key is a space-separated
list of values; each value is of the form
\f[I]memory,disk,vcpu=count\f[] where the memory, disk and vcpu are
the values for the current spec, and count is how many instances of
this spec can be created. A complete value for this variable could
be: \f[B]4096,102400,2=225 2560,102400,2=20 512,102400,2=21\f[].
.RS
.RE
.TP
.B KM_USED_CPU, KM_USED_NPU, KM_USED_MEM, KM_USED_DSK
These represents the metrics of used resources at the start of the
computation (only for tiered allocation mode). The NPU value is
"normalized" CPU count, i.e. the number of virtual CPUs divided by
the maximum ratio of the virtual to physical CPUs.
.RS
.RE
.TP
.B KM_POOL_CPU, KM_POOL_NPU, KM_POOL_MEM, KM_POOL_DSK
These represents the total resources allocated during the tiered
allocation process. In effect, they represent how much is readily
available for allocation.
.RS
.RE
.TP
.B KM_UNAV_CPU, KM_POOL_NPU, KM_UNAV_MEM, KM_UNAV_DSK
These represents the resources left over (either free as in
unallocable or allocable on their own) after the tiered allocation
has been completed. They represent better the actual unallocable
resources, because some other resource has been exhausted. For
example, the cluster might still have 100GiB disk free, but with no
memory left for instances, we cannot allocate another instance, so
in effect the disk space is unallocable. Note that the CPUs here
represent instance virtual CPUs, and in case the \f[I]--max-cpu\f[]
option hasn\[aq]t been specified this will be -1.
.RS
.RE
.TP
.B ALLOC_USAGE
The current usage represented as initial number of instances
divided per final number of instances.
.RS
.RE
.TP
.B ALLOC_COUNT
The number of instances allocated (delta between FIN_INST_CNT and
INI_INST_CNT).
.RS
.RE
.TP
.B ALLOC_FAIL*_CNT
For the last attemp at allocations (which would have increased
FIN_INST_CNT with one, if it had succeeded), this is the count of
the failure reasons per failure type; currently defined are
FAILMEM, FAILDISK and FAILCPU which represent errors due to not
enough memory, disk and CPUs, and FAILN1 which represents a non N+1
compliant cluster on which we can\[aq]t allocate instances at all.
.RS
.RE
.TP
.B ALLOC_FAIL_REASON
The reason for most of the failures, being one of the above FAIL*
strings.
.RS
.RE
.TP
.B OK
A marker representing the successful end of the computation, and
having value "1". If this key is not present in the output it means
that the computation failed and any values present should not be
relied upon.
.RS
.RE
.PP
If the tiered allocation mode is enabled, then many of the
INI_/FIN_ metrics will be also displayed with a TRL_ prefix, and
denote the cluster status at the end of the tiered allocation run.
.SH OPTIONS
.PP
The options that can be passed to the program are as follows:
.TP
.B --memory \f[I]mem\f[]
The memory size of the instances to be placed (defaults to 4GiB).
.RS
.RE
.TP
.B --disk \f[I]disk\f[]
The disk size of the instances to be placed (defaults to 100GiB).
.RS
.RE
.TP
.B --req-nodes \f[I]num-nodes\f[]
The number of nodes for the instances; the default of two means
mirrored instances, while passing one means plain type instances.
.RS
.RE
.TP
.B --vcpus \f[I]vcpus\f[]
The number of VCPUs of the instances to be placed (defaults to 1).
.RS
.RE
.TP
.B --max-cpu=\f[I]cpu-ratio\f[]
The maximum virtual to physical cpu ratio, as a floating point
number between zero and one. For example, specifying
\f[I]cpu-ratio\f[] as \f[B]2.5\f[] means that, for a 4-cpu machine,
a maximum of 10 virtual cpus should be allowed to be in use for
primary instances. A value of one doesn\[aq]t make sense though, as
that means no disk space can be used on it.
.RS
.RE
.TP
.B --min-disk=\f[I]disk-ratio\f[]
The minimum amount of free disk space remaining, as a floating
point number. For example, specifying \f[I]disk-ratio\f[] as
\f[B]0.25\f[] means that at least one quarter of disk space should
be left free on nodes.
.RS
.RE
.TP
.B -p, --print-nodes
Prints the before and after node status, in a format designed to
allow the user to understand the node\[aq]s most important
parameters.
.RS
.PP
It is possible to customise the listed information by passing a
comma-separated list of field names to this option (the field list
is currently undocumented), or to extend the default field list by
prefixing the additional field list with a plus sign. By default,
the node list will contain the following information:
.TP
.B F
a character denoting the status of the node, with \[aq]-\[aq]
meaning an offline node, \[aq]*\[aq] meaning N+1 failure and blank
meaning a good node
.RS
.RE
.TP
.B Name
the node name
.RS
.RE
.TP
.B t_mem
the total node memory
.RS
.RE
.TP
.B n_mem
the memory used by the node itself
.RS
.RE
.TP
.B i_mem
the memory used by instances
.RS
.RE
.TP
.B x_mem
amount memory which seems to be in use but cannot be determined why
or by which instance; usually this means that the hypervisor has
some overhead or that there are other reporting errors
.RS
.RE
.TP
.B f_mem
the free node memory
.RS
.RE
.TP
.B r_mem
the reserved node memory, which is the amount of free memory needed
for N+1 compliance
.RS
.RE
.TP
.B t_dsk
total disk
.RS
.RE
.TP
.B f_dsk
free disk
.RS
.RE
.TP
.B pcpu
the number of physical cpus on the node
.RS
.RE
.TP
.B vcpu
the number of virtual cpus allocated to primary instances
.RS
.RE
.TP
.B pcnt
number of primary instances
.RS
.RE
.TP
.B scnt
number of secondary instances
.RS
.RE
.TP
.B p_fmem
percent of free memory
.RS
.RE
.TP
.B p_fdsk
percent of free disk
.RS
.RE
.TP
.B r_cpu
ratio of virtual to physical cpus
.RS
.RE
.TP
.B lCpu
the dynamic CPU load (if the information is available)
.RS
.RE
.TP
.B lMem
the dynamic memory load (if the information is available)
.RS
.RE
.TP
.B lDsk
the dynamic disk load (if the information is available)
.RS
.RE
.TP
.B lNet
the dynamic net load (if the information is available)
.RS
.RE
.RE
.TP
.B -O \f[I]name\f[]
This option (which can be given multiple times) will mark nodes as
being \f[I]offline\f[].
This means a couple of things:
.RS
.IP \[bu] 2
instances won\[aq]t be placed on these nodes, not even temporarily;
.RS 2
e.g. the \f[I]replace primary\f[] move is not available if the
secondary node is offline, since this move requires a failover.
.RE
.IP \[bu] 2
these nodes will not be included in the score calculation (except
for the percentage of instances on offline nodes)
.PP
Note that the algorithm will also mark as offline any nodes which
are reported by RAPI as such, or that have "?" in file-based input
in any numeric fields.
.RE
.TP
.B -t \f[I]datafile\f[], --text-data=\f[I]datafile\f[]
The name of the file holding node and instance information (if not
collecting via RAPI or LUXI). This or one of the other backends
must be selected.
.RS
.RE
.TP
.B -S \f[I]filename\f[], --save-cluster=\f[I]filename\f[]
If given, the state of the cluster at the end of the allocation is
saved to a file named \f[I]filename.alloc\f[], and if tiered
allocation is enabled, the state after tiered allocation will be
saved to \f[I]filename.tiered\f[].
This allows re-feeding the cluster state to either hspace itself
(with different parameters) or for example hbal.
.RS
.RE
.TP
.B -m \f[I]cluster\f[]
Collect data directly from the \f[I]cluster\f[] given as an
argument via RAPI. If the argument doesn\[aq]t contain a colon (:),
then it is converted into a fully-built URL via prepending
\f[C]https://\f[] and appending the default RAPI port, otherwise
it\[aq]s considered a fully-specified URL and is used as-is.
.RS
.RE
.TP
.B -L [\f[I]path\f[]]
Collect data directly from the master daemon, which is to be
contacted via the luxi (an internal Ganeti protocol). An optional
\f[I]path\f[] argument is interpreted as the path to the unix
socket on which the master daemon listens; otherwise, the default
path used by ganeti when installed with
\f[I]--localstatedir=/var\f[] is used.
.RS
.RE
.TP
.B --simulate \f[I]description\f[]
Instead of using actual data, build an empty cluster given a node
description. The \f[I]description\f[] parameter must be a
comma-separated list of five elements, describing in order:
.RS
.IP \[bu] 2
the allocation policy for this node group
.IP \[bu] 2
the number of nodes in the cluster
.IP \[bu] 2
the disk size of the nodes, in mebibytes
.IP \[bu] 2
the memory size of the nodes, in mebibytes
.IP \[bu] 2
the cpu core count for the nodes
.PP
An example description would be
\f[B]preferred,B20,102400,16384,4\f[] describing a 20-node cluster
where each node has 100GiB of disk space, 16GiB of memory and 4 CPU
cores. Note that all nodes must have the same specs currently.
.PP
This option can be given multiple times, and each new use defines a
new node group. Hence different node groups can have different
allocation policies and node count/specifications.
.RE
.TP
.B --tiered-alloc \f[I]spec\f[]
Besides the standard, fixed-size allocation, also do a tiered
allocation scheme where the algorithm starts from the given
specification and allocates until there is no more space; then it
decreases the specification and tries the allocation again. The
decrease is done on the matric that last failed during allocation.
The specification given is similar to the \f[I]--simulate\f[]
option and it holds:
.RS
.IP \[bu] 2
the disk size of the instance
.IP \[bu] 2
the memory size of the instance
.IP \[bu] 2
the vcpu count for the insance
.PP
An example description would be \f[I]10240,8192,2\f[] describing an
initial starting specification of 10GiB of disk space, 4GiB of
memory and 2 VCPUs.
.PP
Also note that the normal allocation and the tiered allocation are
independent, and both start from the initial cluster state; as
such, the instance count for these two modes are not related one to
another.
.RE
.TP
.B -v, --verbose
Increase the output verbosity. Each usage of this option will
increase the verbosity (currently more than 2 doesn\[aq]t make
sense) from the default of one.
.RS
.RE
.TP
.B -q, --quiet
Decrease the output verbosity. Each usage of this option will
decrease the verbosity (less than zero doesn\[aq]t make sense) from
the default of one.
.RS
.RE
.TP
.B -V, --version
Just show the program version and exit.
.RS
.RE
.SH EXIT STATUS
.PP
The exist status of the command will be zero, unless for some
reason the algorithm fatally failed (e.g. wrong node or instance
data).
.SH BUGS
.PP
The algorithm is highly dependent on the number of nodes; its
runtime grows exponentially with this number, and as such is
impractical for really big clusters.
.PP
The algorithm doesn\[aq]t rebalance the cluster or try to get the
optimal fit; it just allocates in the best place for the current
step, without taking into consideration the impact on future
placements.
.SH SEE ALSO
.PP
\f[B]hbal\f[](1), \f[B]hscan\f[](1), \f[B]hail\f[](1),
\f[B]ganeti\f[](7), \f[B]gnt-instance\f[](8), \f[B]gnt-node\f[](8)
.SH COPYRIGHT
.PP
Copyright (C) 2009, 2010, 2011 Google Inc. Permission is granted to
copy, distribute and/or modify under the terms of the GNU General
Public License as published by the Free Software Foundation; either
version 2 of the License, or (at your option) any later version.
.PP
On Debian systems, the complete text of the GNU General Public
License can be found in /usr/share/common-licenses/GPL.
